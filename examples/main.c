/**
 * ini parser
 * 
 * 2020, Simon Wunderlin
 */

#ifdef __cplusplus 
#include <iostream>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "../src/ini.h"

int main(int argc, char **argv, char** envp) {

	// check parameters
	if (argc != 2) {
		printf("Usage: %s <ini_file>\n", argv[0]);
		return EXIT_FAILURE;
	}

#ifdef __cplusplus 

	// open
	string inifile = string(argv[1]);
	IniParser ini{};
	int err = ini.open(inifile);

	// check success
	if (err != 0) {
		fprintf(stderr, "Error opening file: '%s', %d\n", inifile.c_str(), 
		        err);
		
		return 2;
	}

	// list all sections
	list<string> sections = ini.sections();
	list<string>::iterator it;
	for (auto const& i : sections) {
		cout << "[" << i << "]" << endl;

		list<string> keys = ini.keys(i);
		list<string>::iterator iit;
		for (auto const& key : keys) {
			cout << key << " = " <<  ini.get(i, key) << endl;
		}
	}


	return EXIT_SUCCESS;

#else

	char *inifile;
	int errnum = 0;
	FILE *fp;
	
	// try to open ini file
	inifile = argv[1];
	fp = fopen(inifile, "rb");
	if (fp == NULL) {

		errnum = errno;
		fprintf(stderr, "Error opening file: '%s', %d, %s\n", inifile, 
		        errnum, strerror(errnum));
		
		return 2;
	} 
	
	ini_section_list_t *ini = ini_parse(fp);
	fclose(fp);

	if (ini == NULL) {
		fprintf(stderr, "Failed to parse ini file: '%s', %d, %s\n", inifile,
		        errnum, strerror(errnum));
		return 3;
	}

	// show contents
	int i, ii;
	for (i=0; i<ini->length; i++) {
		printf("S: %s\n", ini->sections[i]->name);

		for(ii=0; ii < ini->sections[i]->length; ii++) {
			printf("-> '%s': '%s'\n", ini->sections[i]->items[ii]->name,
			                          ini->sections[i]->items[ii]->value);
		}
	}

	// find specific item
	ini_section_t *sect = ini_find_section(ini, "sec 2");
	if (sect != NULL) {
		printf("Section name: %s\n", sect->name);

		ini_item_t *i = ini_find_key(sect, "k21 ");
		if (i != NULL) {
			printf("-----> '%s': '%s'\n", i->name, i->value);
		}
	}

	ini_free(ini);

	return EXIT_SUCCESS;
#endif

}
