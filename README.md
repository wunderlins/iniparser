# Ini file parser

## iniparse

a simple C ini file parser (optionally with a C++ interface, if compiled with `#ifdef __cplusplus`). This implementation can only read ini files.

All items must be in a section. key/value paris declared 
before the first `[section]` in the file are ignored.

## Usage

Example usage, see [main.c](main.c).

### Minimal C example:

```c
// read ini file
FILE *fp;
fp = fopen(inifile, "r");
ini_section_list_t *ini = ini_parse(fp);
fclose(fp);

// get a section, and then an item
ini_section_t *sect = ini_find_section(ini, "sec 2");
ini_item_t *i = ini_find_key(sect, "k21 ");
printf("'k21 ': %s\n", i->value);
```

### Minimal C++ example:

```C++
// read ini file
IniParser ini{};
int err = ini.open(inifile);

// get a value, will return "" when not found
std::string v = ini.get("section name", "the key");

// get all section names
std::list<string> sections = ini.sections();

// get all keys from a section
std::list<string> keys = ini.keys("section name");
```

## Building

`gcc` will build the `C` implementation only, `g++` will build the `C` and `C++` implementation.

- `make`: creates release
- `make debug`: with debug symbols
- `make valgrind`: with debug symbols, creates `valgrind-out.txt` in current directory
