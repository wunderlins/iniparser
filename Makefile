.PHONY: main

ifdef CPLUSPLUS
CC      = g++
else
CC      = gcc
endif
CFLAGS  = -Wall

SRCS = src/ini.c
OBJS = $(SRCS:.c=.o)

ifeq ($(BUILD),debug)   
	# "Debug" build - no optimization, and debugging symbols
	CFLAGS += -O0 -g
else
	# "Release" build - optimization, and no debug symbols
	CFLAGS += -O2 -s -DNDEBUG
endif

all: main

main: $(OBJS)
	$(CC) $(CFLAGS) -o $@ ./examples/$@.c $(OBJS)

debug:
	make "BUILD=debug"

.c.o:
	$(CC) $(CFLAGS) -c $< -o $@

clean:
	@rm -f c cpp *.opp *.o $(OBJS) | true

valgrind: clean debug
	#valgrind --leak-check=full \
	#	--show-leak-kinds=all \
	#	--track-origins=yes \
	#	--verbose \
	#	--log-file=valgrind-out.txt \
	#	./main test.ini
	valkyrie --track-origins=yes ./c resources/test.ini
