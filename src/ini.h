/**
 * @file ini.h
 * @author your name (you@domain.com)
 * @brief Ini file parsers
 * @version 0.1
 * @date 2022-05-07
 * 
 * @copyright Copyright (c) 2022
 * 
 */
#ifndef _INI_H_
#define _INI_H_

// set for testing
//#ifndef __cplusplus
//#define __cplusplus
//#endif

#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef __cplusplus 

#include <string>
#include <list>
using namespace std;

extern "C" {
#endif

/**
 * we are dynamically pre-allocating child items so we don't have
 * to call realloc() on every item added. If you have large structures
 * with many items settin this to a higher value might benefit 
 * performance (less realloc calls).
 * 
 * default: 10
 */
#define ALLOC_NUM_ITEMS 10

/**
 * @brief last known position of element boundaries
 * 
 * This structure holds the last known position of element boundaries
 */
typedef struct {
	/// position in bytes of the secrtionname's start in the file
	size_t section_start;
	/// position in bytes of the secrtionname's end in the file
	size_t section_end;
	/// item's start position	
	size_t item_start;
	/// items end position
	size_t item_end;
	/// position of the items key/value delimiter '='
	size_t item_equal;
	/// item's trailing commend (not implemented?)
	size_t item_comment;
} last_pos_t;

/**
 * @brief ini item (key + value)
 * 
 * This structure defines a key/value pair of an ini file
 */
typedef struct {
	/// start position in file
	int start; 
	/// end position in file
	int end;
	/// key name
	char* name;
	/// item value
	char* value;
} ini_item_t;

/**
 * @brief all k/v combined in a section
 * 
 * this structure holds a section with N items 
 */
typedef struct {
	/// name
	char* name;
	/// number of items in this section
	int length;  
	/// max allocated items in `this->items`
	int size;
	/// key/value pairs
	ini_item_t **items;
} ini_section_t;

/**
 * @brief container for parsed ini data
 * 
 * Top level strcture, holds N sections.
 */
typedef struct {
	/// number of sections
	int length;   
	/// max allocated space in sections 
	int size;
	/// collection of sections
	ini_section_t **sections;
} ini_section_list_t;

/**
 * @brief parse ini file
 * 
 * @param [in] fp file pointer to the ini file
 * @return ini_section_list_t* or NULL on error
 */
ini_section_list_t *ini_parse(FILE *fp);

/**
 * @brief free memory of allocated ini data
 * 
 * @param [in] ini 
 */
void ini_free(ini_section_list_t *ini);

/**
 * @brief find a section in parsed file
 * 
 * find a section in a parsed ini file by name.
 * Returns nULL if not found.
 * 
 * @param ini pointer to the parsed ini file
 * @param section_name case sensitive section name
 * @return ini_section_t* or NULL if not found
 */
ini_section_t *ini_find_section(ini_section_list_t *ini, 
                                const char* section_name);

/**
 * @brief find key in section
 * 
 * Finds a key ba nyme in a section. Returns NULL if not found.
 * 
 * @param s pointer to section (@see ini_find_section)
 * @param key string name of section (search is case sensitive)
 * @return ini_item_t* or NULL if not found
 */
ini_item_t *ini_find_key(ini_section_t *s, const char* key);

// get a value for a specific item in a section.
// returns NULL if not found

/**
 * @brief fet a specific value from an ini file
 * 
 * @param ini pointer to parsed ini file structure
 * @param section string name of the section (case sensitive)
 * @param key string name of the key in the section (case sensitive)
 * @return char* or NULL if not found
 */
char *ini_get_value(ini_section_list_t *ini,
                const char* section, const char* key);

#ifdef __cplusplus 
} // end extern "C"

// C++ implementation

/**
 * @brief C++ implementation of C ini parser
 */
class IniParser {
protected:

public:
    // base variables
    int errnum = 0;
    char *inifile;
    FILE *fp;
    ini_section_list_t *section_list;


    IniParser();
    //IniParser(string file);
    ~IniParser();

    //int open();
    int open(string file);

    // lookup functions
	bool exists(const string section, const string key);
    string get(const string section, const string key, string default_value = "");
    list<string> sections();
    list<string> keys(const string section_name);
};

#endif

#endif // _INI_H_
